<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
localhost:8000/ -> controller home @ index
localhost:8000/home -> controller home @ index
localhost:8000/product -> controller product @ index
localhost:8000/product/detail -> controller product @ detail 
localhost:8000/contact -> controller page @contact*/

use App\Http\Middleware\CheckAdmin;
use App\Http\Middleware\CheckMember;


Auth::routes();

Route::get("/logout",function()
{
	return view('layouts.app');
});

Route::get('/', 'HomeController@index');
Route::get('/home','HomeController@index');
Route::prefix('product')->group(function () {
	Route::get('/', 'ProductController@index');
    Route::get('/detail','ProductController@detail');
});



/* Route Admin*/

Route::prefix('dashboard')->middleware(CheckAdmin::class)->group(function () {
	Route::get('/', 'dashboard\HomeController@home');
	
	
	// Route::get('/user', 'dashboard\UserController@user');
	Route::prefix('/user')->group(function(){
		Route::get('/','dashboard\UserController@home');
		Route::post('/create','dashboard\UserController@create');
		Route::get('/edit/{id}','dashboard\UserController@edit');
		Route::post('/update/{id}','dashboard\UserController@update');
		Route::get('/delete/{id}','dashboard\UserController@delete');
		
	});
	Route::prefix('/video')->group(function(){
		Route::get('/','dashboard\VideoController@home');
		Route::post('/create','dashboard\VideoController@create');
		Route::get('/edit/{id}','dashboard\VideoController@edit');
		Route::post('/update/{id}','dashboard\VideoController@update');
		Route::get('/delete/{id}','dashboard\VideoController@delete');
		
	});
	Route::prefix('/rent_video')->group(function(){
		Route::get('/','dashboard\Rent_videoController@home');
		Route::post('/create','dashboard\Rent_videoController@create');
		Route::get('/get_all_rent_video','dashboard\Rent_videoController@get_all_rent_video');
		Route::get('/edit/{id}','dashboard\Rent_videoController@edit');
		Route::post('/update','dashboard\Rent_videoController@update');
		Route::get('/delete/{id}','dashboard\Rent_videoController@delete');
		
	});


	Route::prefix('/category')->group(function(){
		Route::get('/','dashboard\CategoryController@home');
		Route::post('/create','dashboard\CategoryController@create');
		Route::get('/edit/{id}','dashboard\CategoryController@edit');
		Route::post('/udpate/{id}','dashboard\CategoryController@update');
		Route::get('/delete/{id}','dashboard\CategoryController@delete');
		Route::get('/sortup/{id}','dashboard\CategoryController@sortup');
		Route::get('/sortdown/{id}','dashboard\CategoryController@sortdown');
	});

});




/* Route Member */

Route::prefix('member')->middleware(CheckMember::class)->group(function () {
	Route::get('/', 'Rent_videoController@home');	
	Route::post('/update', 'Rent_videoController@update');	
	Route::post('/create', 'Rent_videoController@create');	
	Route::get('/watch/{id}', 'Rent_videoController@watch');	
});
Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('/cart')->group(function(){
		Route::get('/','CartController@home');
	});
