@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">List Video</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Name</th>
                          <th scope="col">Category</th>
                          <th scope="col">Create at</th>
                          <th scope="col">Watch</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php $no=1; ?>
                    @foreach ($list_video as $row ) 
                    <tr>
                      <th scope="row">{{$no++}}</th>
                      <td class="name_list_video">{{$row->name}}</td>
                      <td class="category_list_video">{{$row->category->name}}</td>
                      <td>{{$row->created_at}}</td>
                      <td>
                        <button class="btn btn-primary btn-sm btn_req_list" data-id="{{$row->id}}">request</button>
                        

                      </td>
                  </tr>
                     @endforeach
              </tbody>
          </table>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="card-header">History Video Requested</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">name</th>
                          <th scope="col">Video_id</th>
                          <th scope="col">billing_time</th>
                          <th scope="col">status</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php $no=1; ?>
                    @foreach ($hist_watched as $row ) 
                   <tr>
                      <th scope="row">{{$no++}}</th>
                      <td class="name">{{$row->name}}</td>
                      <td class="video_id">{{$row->video_id}}</td>
                      <td class="billing_time">{{$row->billing_time}}</td>
                      <td class="desc">
                        @if ($row->status==1 )

                        <a href="member/watch/{{$row->id}}" class="btn btn-success btn-sm">watch</a>


                        
                       @elseif ($row->status==2 )
                       <button class="btn btn-primary btn-sm" data-id="">request</button> 
                       @else  ($row->status==3 )
                       <span type="button" class="badge badge-secondary" style="height:30px; width: 60px;">pending</span> 

                       @endif
                     </td>
                 </tr>
                     @endforeach
             
              </tbody>
          </table>
                </div>
            </div>
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script type="text/javascript">
          $(document).ready(function(){

            $('.btn_req').click(function() {
              var name = $(this).parent().parent().find('.name').html();
              var status = $(this).attr('data-id');
              var video_id = $(this).parent().parent().find('.video_id').html();

               alert(status)
                    $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                 $.ajax({
                    method: "POST",
                    url: "{{url('member/create')}}",
                    dataType : 'json',
                    data : 
                    {
                      "name":name,
                      "status":status,
                      "video_id":video_id,
                    },
                    
                    success:function(data)
                    {
                       alert(data)
                      window.location=('{{url("member")}}')
                    }
                   
                })

             });
          });


          $(document).ready(function(){
            $('.btn_req_list').click(function() {
              var name = $(this).parent().parent().find('.name_list_video').html();
              var video_id = $(this).attr('data-id');
          

              alert(name)
              $.ajaxSetup({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
              });
              $.ajax({
                method: "POST",
                url: "{{url('member/create')}}",
                dataType : 'json',
                data : 
                {
                  "name":name,
                  "status":3,
                  "video_id":video_id,
                },

                success:function(data)
                {
                 alert(data)
                 window.location=('{{url("member")}}')
               }

             })

            });
          });




        </script>
</div>
@endsection
