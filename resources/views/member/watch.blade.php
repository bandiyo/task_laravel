


@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    
<div id="jquery-script-menu">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h5>TITTLE OF VIDEO : {{$videos->name}}</h5>
				<p class="lead">A dead simple countdown timerstarts or when a specific time is due.</p>
			</div>
			
			<p class="lead counter"></p>
			<div class="clear"></div>
			<hr />
			
		</div>
		<div class="row">
			<div class="col-md-12">
				<button class="btn btn-success" data-id="{{$videos->id}}">Request watch to Admin</button>				
			</div>
		</div>
	</div>
	
</div>



<?php $videos['video_id'];?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function () {
		var vid = <?php echo $videos['video_id']; ?>

		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$('.btn').click(function () {
			alert('halo')
			var id_rent_video = $(this).data('id')
			alert(id_rent_video)
			$.ajax({
				url: '/member/update/',
				type: 'POST',
				data :{
					"_token": "{{ csrf_token() }}",
					"id":id_rent_video,
				},
				dataType: 'json',
				success: function (data) {
					alert(data)
					window.location=('{{url("member")}}')
				},
			});
		});
	}); 
	
</script>

</script>
<?php	

$convert = json_encode($videos['billing_time']);


?>

<script src="{{ asset('admin/js/countdowntimer.js')}}"></script>
<script>
	$(function() {
		var val = <?php echo $convert; ?>

		$('.counter').setCountDownTimer({
			time:val ,
			button: $('.btn'),
			countDownText: 'Remaining time:',
			afterCountText: '<h1 style="color:red;text-align:center;">Your Time is Up.!</h1>',
			
		});
	});
</script>





</script>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
