@extends('admin.template')
<style type="text/css">
    .invalid{
        color: red;
    }

    .alert-custom{
      background-color:#FFD700;
      color:#fff;
      transition: opacity 0.9s;
  }
  .custom:hover{
    display:none!important;
}
</style>


@section('content')     

<div class="col-md-12">
    <!-- DATA TABLE -->
    <h3 class="title-5 m-b-35">list Reques to Rent Video</h3>
    <div class="table-data__tool">
        <div class="table-data__tool-left">
            <div class="rs-select2--light rs-select2--md">
                <select class="js-select2" name="property">
                    <option selected="selected">All Properties</option>
                    <option value="">Option 1</option>
                    <option value="">Option 2</option>
                </select>
                <div class="dropDownSelect2"></div>
            </div>
            <div class="rs-select2--light rs-select2--sm">
                <select class="js-select2" name="time">
                    <option selected="selected">Today</option>
                    <option value="">3 Days</option>
                    <option value="">1 Week</option>
                </select>
                <div class="dropDownSelect2"></div>
            </div>
            <button class="au-btn-filter">
                <i class="zmdi zmdi-filter-list"></i>filters</button>
            </div>
            <div class="table-data__tool-right">

               <!-- Button trigger modal -->
                                       <!--  <button class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#exampleModal">
                                            <i class="zmdi zmdi-plus"></i>add item
                                        </button> -->
                                        
                                        <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                                            <select class="js-select2" name="type">
                                                <option selected="selected">Export</option>
                                                <option value="">Option 1</option>
                                                <option value="">Option 2</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                    @if(session('success'))
                                    <div class="alert alert-custom" role="alert">
                                        {{session('success')}}
                                    </div>
                                    @endif
                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <label class="au-checkbox">
                                                        <input type="checkbox">
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </th>
                                                <th>id</th>
                                                <th>name video</th>
                                                <th>user id</th>
                                                <th>video id</th>
                                                <th>billing time</th>
                                                <th>status</th>
                                                <th style="text-align: center;">action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($rent_videos as $row )   
                                            <tr class="tr-shadow">
                                                <td>
                                                    <label class="au-checkbox">
                                                        <input type="checkbox">
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </td>
                                                <td>{{$row->id}}</td>
                                                <td>
                                                    <span class="block-email"> {{$row->name}}</span>
                                                </td>

                                                <td class="desc">{{$row->user_id}}</td>
                                                <td><span class="status--process block-email">{{$row->video_id}}</span></td>
                                                <td class="desc">{{$row->billing_time}}</td>
                                                <!-- <td class="desc">{{$row->status}}</td> -->

                                                <td class="desc">
                                                   @if ($row->status==1 )<span class="badge badge-success">ACCEPTED</span>
                                                   @else <span class="badge badge-warning">REQUEST</span> @endif
                                               </td>
                                               <!-- <td>{{$row->created_at}}</td> -->
                                               <td>
                                                <div class="table-data-feature">
                                                    <!-- Button trigger modal -->

                                                    <button class="item"  title="Edit" data-toggle="modal" data-target="#modalCustom"  data-id="{{$row->id}}">
                                                        <i class="zmdi zmdi-edit"></i>
                                                    </button>
                                                    <button class="item" data-toggle="tooltip" data-placement="top" title="Delete" onclick="confirmDelete('{{$row->id}}')">
                                                        <i class="zmdi zmdi-delete"></i>
                                                        
                                                    </button>

                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="spacer"></tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <!-- END DATA TABLE -->
                        </div>


                        <!-- Modal  Tambah Data-->
                        <div class="modal fade" id="modalCustom" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Edit Status & Billing Time</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <div class="modal-body">
                            <form id="formData">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <input type="text" name="id" class="form-control rent_vid_id" value="0" readonly="" style="height:30px; width:40px">
                                </div>
                                <div>
                                    <label for="exampleFormControlInput1">Status Request</label>
                                    <select class="form-control" name="status">                   
                                        <option class="0">- choose status -</option>
                                        <option value="1">aceppted</option>
                                        <option value="2">request</option>
                                    </select>
                                    <span class="invalid"><i></i></span>
                                </div>
                                 <div class="form-group">
                                     <label for="exampleFormControlInput1">Input Time. example (00:02:00)</label>
                                     <input type="text" id="password-input" name="billing_time" placeholder="Input minutes time" class="form-control @error('name') is-invalid invalid @enderror">
                                     @error('billing_time')
                                     <span class="invalid"><i>{{$message}}</i></span>
                                     @enderror
                                 </div>
                                <div class="modal-footer">
                                    <button type="reset" class="btn btn-secondary" data-dismiss="modal">Reset</button>
                                    <button type="sumbit" class="btn btn-success">Save</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <script> window.setTimeout(function() 
            { 
                $(".alert-custom").fadeTo(100, 0).slideUp(500, function() { $(this).remove();  });
            }, 2000);

            function confirmDelete(id){
                var pesan = confirm('Are you sure want to delete this data ?')

                if(pesan == true)
                {
                    window.location=('{{url("dashboard/rent_video/delete/")}}/'+id)
                }
                else{
                    return false
                }
            }
        </script>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script type="text/javascript">
          $(document).ready(function(){
            $('[data-target="#modalCustom"').click(function() {
              var id = $(this).attr('data-id');
               // alert(id)
                $('#formData').parent().find('.rent_vid_id').val(id);
                
            });

           
           $('#formData').submit( function(event) {
                event.preventDefault();
                var data = $(this).serialize();
                // alert(data)
                $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                 $.ajax({
                    method: "POST",
                    url: "{{url('dashboard/rent_video/update')}}",
                    dataType : 'json',
                    data : data,
                    
                    success:function(data)
                    {
                        alert('success, Data updated !')
                        $('#modalCustom').modal('hide');
                         window.location=('{{url("dashboard/rent_video/")}}')
                    }
                   
                })
                
             
            })


        })
    </script>
    @endsection