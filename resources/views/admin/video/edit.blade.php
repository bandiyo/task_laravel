@extends('admin.template')
<style type="text/css">
.invalid{
    color: red;
}
</style>
@section('content')

    <div class="col-lg-8">
                                @if(session('sukses'))
                                    <div class="alert alert-success" role="alert">
                                    {{session('sukses')}}
                                    </div>
                                @endif
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>Video</strong> Update
                                        </div>
                                        <div class="card-body card-block">
                                                
                                            <form action="{{url('dashboard/video/update')}}/{{$row->id}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                            {{csrf_field()}}
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="text-input" class=" form-control-label">Name</label>
                                                    </div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="text-input" name="name" class="form-control @error('name') is-invalid invalid @enderror"value="@if(old('name')){{$row->name}}@else{{$row->name}} @endif">
                                                        </div>
                                                         @error('name')
                                                            <span class="invalid"><i>{{$message}}</i></span>
                                                        @enderror
                                                </div>
                                           
                                                        <!-- Single button -->
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="email-input" class=" form-control-label">Category</label>
                                                    </div>
                                                <div class="col-12 col-md-9">
                                                <select class="form-control" name="category_id">                   
                                                    @foreach ($select_category as $row )
                                                    <option value="@if(old('id')){{$row->id}}@else{{$row->id}} @endif">{{$row->name}}</option>
                                                    @endforeach
                                                    @error('category_id')
                                                            <span class="invalid"><i>{{$message}}</i></span>
                                                    @enderror
                                                </select>
                                                </div>
                                                </div>
                                                
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="" class=" form-control-label">Link</label>
                                                    </div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="password-input" name="link" placeholder="Input link" class="form-control @error('link') is-invalid invalid @enderror" value="@if(old('link')){{$row->link}}@else{{$row->link}}@endif">
                                                    </div>
                                                        @error('link')
                                                            <span class="invalid"><i>{{$message}}</i></span>
                                                        @enderror
                                                </div>

                                              
                                        
                  
                                                     <div class="card-footer">
                                                        <button type="submit" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                                            <i class="fa fa-dot-circle-o"></i> Update
                                                        </button>
                                                        <button type="Reset" class="au-btn au-btn-icon au-btn--yellow au-btn--small">
                                                            <i class="fa fa-ban"></i> Reset
                                                        </button>
                                                    </div>
                                            </form>
                                            
                                        </div>
                                       
                                    </div>
                                    
    </div>

@endsection