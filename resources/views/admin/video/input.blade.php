@extends('admin.template')

<style type="text/css">
.invalid{
    color: red;
}
</style>

@section('content')

    <div class="col-lg-8">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>Video</strong> Input
                                        </div>
                                        <div class="card-body card-block">
                                            <form action="{{url('dashboard/product/input')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                            @csrf
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="text-input" class=" form-control-label">Nama</label>
                                                    </div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="text-input" name="gambar" readonly="" class="form-control">
                                                        @error('nama')
                                                              <span class="invalid"><i>{{$message}}</i></span>
                                                        @enderror

                                                        </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="email-input" class=" form-control-label">Nama</label>
                                                    </div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="email-input" name="name" placeholder="Nama barang" class="form-control @error('name') is-invalid invalid @enderror">
                                                        @error('name')
                                                            <span class="invalid"><i>{{$message}}</i></span>
                                                        @enderror

                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="" class=" form-control-label">Stock</label>
                                                    </div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="number" id="password-input" name="stock" placeholder="Jumlah stock" class="form-control @error('name') is-invalid invalid @enderror">
                                                        @error('stockt')
                                                            <span class="invalid"><i>{{$message}}</i></span>
                                                        @enderror
                                                    </div>
                                                </div>
          
                                            
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="textarea-input" class=" form-control-label">Keterangan</label>
                                                    </div>
                                                    <div class="col-12 col-md-9">
                                                        <textarea name="textarea-input" id="textarea-input" name="keterangan" rows="5" placeholder="Masukan keterangan barang" class="form-control @error('name') is-invalid invalid @enderror"></textarea>
                                                        @error('keterangan')
                                                            <span class="invalid"><i>{{$message}}</i></span>
                                                        @enderror

                                                    </div>
                                                </div>
                                            
                  
                                            </form>
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-primary btn-sm">
                                                <i class="fa fa-dot-circle-o"></i> Submit
                                            </button>
                                            <button type="reset" class="btn btn-danger btn-sm">
                                                <i class="fa fa-ban"></i> Reset
                                            </button>
                                        </div>
                                    </div>
                                    
    </div>

@endsection