@extends('admin.template') <style type="text/css"> .invalid{ color: red; }

.alert-custom{
  background-color:#FFD700;
  color:#fff;
  transition: opacity 0.9s;
}
 .custom:hover{
    display:none!important;
}
</style>


@section('content')     
                      
                            <div class="col-md-12">
                                <!-- DATA TABLE -->
                                <h3 class="title-5 m-b-35">list Users</h3>
                                <div class="table-data__tool">
                                    <div class="table-data__tool-left">
                                        <div class="rs-select2--light rs-select2--md">
                                            <select class="js-select2" name="property">
                                                <option selected="selected">All Properties</option>
                                                <option value="">Option 1</option>
                                                <option value="">Option 2</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                        <div class="rs-select2--light rs-select2--sm">
                                            <select class="js-select2" name="time">
                                                <option selected="selected">Today</option>
                                                <option value="">3 Days</option>
                                                <option value="">1 Week</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                        <button class="au-btn-filter">
                                            <i class="zmdi zmdi-filter-list"></i>filters</button>
                                    </div>
                                    <div class="table-data__tool-right">

                                         <!-- Button trigger modal -->
                                        <button class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#exampleModal">
                                            <i class="zmdi zmdi-plus"></i>add User
                                        </button>
                                        
                                        <div class="rs-select2--dark rs-select2--sm rs-select2--dark2">
                                            <select class="js-select2" name="type">
                                                <option selected="selected">Export</option>
                                                <option value="">Option 1</option>
                                                <option value="">Option 2</option>
                                            </select>
                                            <div class="dropDownSelect2"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive table-responsive-data2">
                                @if(session('success'))
                                    <div class="alert alert-custom" role="alert">
                                    {{session('success')}}
                                    </div>
                                @endif
                                    <table class="table table-data2">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <label class="au-checkbox">
                                                        <input type="checkbox">
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </th>
                                                <th>id</th>
                                                <th>name</th>
                                                <th>email</th>
                                                <th>level</th>
                                                <th>createt at</th>
                                                <th style="text-align: center;">action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($users as $row )   
                                            <tr class="tr-shadow">
                                                <td>
                                                    <label class="au-checkbox">
                                                        <input type="checkbox">
                                                        <span class="au-checkmark"></span>
                                                    </label>
                                                </td>
                                                <td>{{$row->id}}</td>
                                                <td>
                                                    <span class="block-email"> {{$row->name}}</span>
                                                </td>
                                               
                                                <td class="desc">{{$row->email}}</td>
                                                <td><span class="status--process block-email">{{$row->level}}</span></td>
                                                <td>{{$row->created_at}}</td>
                                               
                                                
                                                <td>
                                                    <div class="table-data-feature">
                                                        

                                                        <a href='{{ url("dashboard/user/edit/") }}/{{$row->id}}'>
                                                            <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                            <i class="zmdi zmdi-edit"></i>
                                                            </button>
                                                        </a>
                                                        
                                                        <button class="item" data-toggle="tooltip" data-placement="top" title="Delete" onclick="confirmDelete('{{$row->id}}')">
                                                            <i class="zmdi zmdi-delete"></i>
                                                        </a>
                                                        </button>
                                                        
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="spacer"></tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                               
                                <!-- END DATA TABLE -->
                            </div>
                           
                           <!-- Modal  Tambah Data-->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                  <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                      <div class="modal-body">
                                        <form action="{{ url('dashboard/user/create')}}" method="POST">
                                            {{csrf_field()}}
                                             
                                             <div class="form-group">
                                                <label for="exampleFormControlInput1">Name</label>
                                                <input type="text" name="name" class="form-control @error('name') is-invalid invalid @enderror" id="exampleFormControlInput1" placeholder="Enter name user"  value="{{old('name')}}" required="">
                                                        @error('name')
                                                            <span class="invalid"><i>{{$message}}</i></span>
                                                        @enderror
                                              </div>

                                               <div class="form-group">
                                                <label for="exampleFormControlInput1">Email</label>
                                                <input type="text" name="email" class="form-control @error('email') is-invalid invalid @enderror" id="exampleFormControlInput1" placeholder="Enter email"  value="{{old('email')}}" required="">
                                                        @error('email')
                                                            <span class="invalid"><i>{{$message}}</i></span>
                                                        @enderror
                                              </div>

                                              <div class="form-group">
                                                <label for="exampleFormControlInput1">Passowrd</label>
                                                <input type="password" name="password" class="form-control @error('password') is-invalid invalid @enderror" id="exampleFormControlInput1" placeholder="Enter password"  value="{{old('password')}}" required="">
                                                        @error('password')
                                                            <span class="invalid"><i>{{$message}}</i></span>
                                                        @enderror
                                              </div>

                                              <div>
                                                <label for="exampleFormControlInput1">Level</label>
                                                
                                                <select class="form-control" name="level">                   
                                                    <option class="custom">- Choose lebel -</option>

                                                    <option value="1">Admin</option>
                                                    <option value="2">Member</option>

                                                </select>

                                                @error('name')
                                                <span class="invalid"><i>{{$message}}</i></span>
                                                @enderror

                                            </div>
 
                                              <div class="modal-footer">
                                                <button type="reset" class="btn btn-secondary" data-dismiss="modal">Reset</button>
                                                <button type="sumbit" class="btn btn-success">Save</button>
                                              </div>
                                        </form>
                                      </div>
                                      
                                    </div>
                                  </div>
                                </div>
                                
                                <script> window.setTimeout(function() 
                                    { 
                                        $(".alert-custom").fadeTo(100, 0).slideUp(500, function() { $(this).remove();  });
                                    }, 2000);

                                    function confirmDelete(id){
                                        var pesan = confirm('Are you sure want to delete  ?')

                                        if(pesan == true)
                                        {
                                            window.location=('{{url("dashboard/user/delete/")}}/'+id)
                                        }
                                        else{
                                            return false
                                        }
                                    }
                                </script>
@endsection