@extends('admin.template')
<style type="text/css">
.invalid{
    color: red;
}
</style>
@section('content')

    <div class="col-lg-8">
                                @if(session('sukses'))
                                    <div class="alert alert-success" role="alert">
                                    {{session('sukses')}}
                                    </div>
                                @endif
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>User</strong> Update
                                        </div>
                                        <div class="card-body card-block">
                                                
                                            <form action="{{url('dashboard/user/update')}}/{{$row->id}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                                            {{csrf_field()}}
                                               
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="email-input" class=" form-control-label">Name</label>
                                                    </div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="text" id="email-input" name="name" placeholder="Name" class="form-control @error('name') is-invalid invalid @enderror" value="@if(old('name')){{$row->name}}  @else{{$row->name}} @endif">
                                                    </div>
                                                        @error('name')
                                                            <span class="invalid"><i>{{$message}}</i></span>
                                                        @enderror
                                                </div>
                                                
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="" class=" form-control-label">Email</label>
                                                    </div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="email" id="password-input" name="email" placeholder="Email" class="form-control @error('email') is-invalid invalid @enderror" value="@if(old('email')){{$row->email}}@else{{$row->email}}@endif">
                                                    </div>
                                                        @error('email')
                                                            <span class="invalid"><i>{{$message}}</i></span>
                                                        @enderror
                                                </div>

                                                 <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="" class=" form-control-label">Password</label>
                                                    </div>
                                                    <div class="col-12 col-md-9">
                                                        <input type="password" id="password-input" name="password" placeholder="Password" class="form-control @error('password') is-invalid invalid @enderror" value="">
                                                    </div>
                                                        @error('password')
                                                            <span class="invalid"><i>{{$message}}</i></span>
                                                        @enderror
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col col-md-3">
                                                        <label for="" class=" form-control-label">Level</label>
                                                    </div>
                                                <div class="col-12 col-md-9">
                                                <select class="form-control" name="level">                   
                                                    <option class="custom">- choose level -</option>
                                                    <option value="admin">Admin</option>
                                                    <option value="member">Member</option>

                                                </select>
                                                </div>
                                                </div>
                                                     <div class="card-footer">
                                                        <button type="submit" class="au-btn au-btn-icon au-btn--green au-btn--small">
                                                            <i class="fa fa-dot-circle-o"></i> Update
                                                        </button>
                                                        <button type="Reset" class="au-btn au-btn-icon au-btn--yellow au-btn--small">
                                                            <i class="fa fa-ban"></i> Reset
                                                        </button>
                                                    </div>
                                            </form>
                                            
                                        </div>
                                       
                                    </div>
                                    
    </div>

@endsection
