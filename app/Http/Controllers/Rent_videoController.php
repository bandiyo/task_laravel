<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  App\Rent_video;
use  App\Video;
use Illuminate\Support\Facades\DB;
class Rent_videoController extends Controller
{
    public function home()
    {
        $list_video = Video::all();
        $user_id = Auth::id();

        $hist_watched = Rent_video::select('*')->where('user_id',$user_id)->get();
    	return view('member.home', compact('list_video','hist_watched'));
        
        
    }

    public function create(Request $request)
    {

        $cek_video_id = DB::table('rent_videos')->where('video_id', $request->video_id)->where('user_id',Auth::id())->first(); 
        // dd($cek_video_id->user_id); die();
            if ($cek_video_id!=null&&$cek_video_id->user_id==Auth::id()) {
                if ($cek_video_id->status==3&&$request->status==3) {
                    return json_encode('waiting admin accept your request !');
            }
        }

        
    	$videos = new Rent_video;
    	$videos->video_id = $request->video_id;
    	$videos->name = $request->name;
    	$videos->user_id = Auth::id();
    	$videos->status = $request->status;

    	$videos->save();
        return json_encode('request sent !');
    }

    public function update(Request $request)
    {
        $rent_videos = Rent_video::find($request->id);

        $rent_videos->billing_time = '00:00:00';
        $rent_videos->status = 3;

        $rent_videos->save();
        return json_encode('success, request has ben sent !'); 
    }

    public function watch($id)
    {
        $videos = Rent_video::find($id);
        // dd($videos);
        $user_id = Auth::id();
        // dd($user_id);
        return view('member.watch',['videos'=>$videos]);
    }
}
