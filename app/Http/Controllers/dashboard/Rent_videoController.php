<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  App\Rent_video;
use App\Category;
class Rent_videoController extends Controller
{
    public function home()
    {
    	$rent_videos = Rent_video::all();
      $select_category = Category::all();   
    	// dd($videos);
    	return view('admin.rent_video.home',compact('rent_videos','select_category'));
    }

    public function get_all_rent_video()
    {
      $rent_video['videos'] = Rent_video::all();
     
     return json_encode($rent_video);
    }
      public function update(Request $request)
      {
        $Rent_video = $request->all();
        $rent_video = Rent_video::find($request['id']);
        
        $rent_video->status = $request['status'];
        $rent_video->billing_time = $request['billing_time'];

        $rent_video->save();

        return json_encode('success');


      }
      public function delete($id)
      {
        // dd($id);
        Rent_video::destroy($id);
        return redirect('dashboard/rent_video/')->with('success','Data deleted');
      }

}
