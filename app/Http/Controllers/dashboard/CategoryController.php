<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\category;

class CategoryController extends Controller
{
    public function home()
    {
    	$category = \App\Category::orderBy('sort','desc')->get();
    	return view('admin.category.home',compact('category'));
    }
    public function create(Request $request)
    {
        $this->validate($request,[
            'name'     =>'required|string|max:50'
        ]);

        /*class category 
        {
            var nama="dodi" ; 
            var sort="1" ; 

            function save()
            {
                $this->name ; 
                $this->sort ; 
                INSERT TBL adada name = $this->name , sort = $this->sort 
            }

            function update()
            {

            }

        }*/

        // cari nilai sort terbesar dan +1
        $bigSort = \App\Category::max('sort');
        $addSort =  $bigSort+1;

        //format insert data
        $categories = new category; // buat object pake class category 
        $categories->name = $request->name; // $category pnya atribut yaitu name , atribut tsb di isi data yang dilempar dari $_POST['name'] , tp klo laravel $_POST['name'] = $request->name
        $categories->sort = $addSort;

        $categories->save(); // proses save
        // --- END FORMAT -- 

    
        return redirect('dashboard/category')->with('sukses','Data berhasil di input');
    }

    public function sortup($id)
    {

        $terpilih = \App\Category::find($id); //5
        $cekBigSort = \App\Category::max('sort'); //5

        if ($terpilih->sort >= $cekBigSort){
        
            return back()->with('sukses','Data anda di posisi paling atas');

        }else{  
             $sortSebelum = $terpilih->sort ; //3
             $sortBaru = $terpilih->sort + 1; // sort 4

             //kita cari atasnya
             $dataAtasnya = \App\Category::where('sort',$sortBaru)->first(); 

             // yg tdnya 3 jadi 4
             $terpilih->sort = $sortBaru ; 
            
             $terpilih->save() ; 

             //4nya ada 2 , jadinya pake id 
             $dataAtasnya->sort =  $sortSebelum ;
             $dataAtasnya->save() ; 
             
             $data = \App\Category::select('name')->find($id);
             return back()->with('sukses','Data '.$data->name.' berhasil dinaikan'); 
        }

         

    }



    public function sortdown($id)
    {
            $pilih = \App\Category::find($id);
            $sortSebelum = $pilih->sort;
            $sort_down = $pilih->sort-1;
            $data_bawahnya = \App\Category::where('sort',$sort_down)->first();

            $pilih->sort = $data_bawahnya->sort;
            $pilih->save();

            $data_bawahnya->sort = $sortSebelum;
            $data_bawahnya->save();
            $data = \App\Category::select('name')->find($id); 
            return back()->with('sukses','Data '.$data->name.' berhasil di turunkan'); 
       
       
    }
}
