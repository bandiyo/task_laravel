<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function home(){
    	$users = DB::table('users')->where('level', 'member')->get();
    	// dd($user);
    	return view('admin.user.home', compact('users'));
    }

    public function create(Request $request)
    {
        $users = new User();
        $users->name = $request->name;
        $users->email = $request->email;
        $users->password = bcrypt($request->password);
        $users->level =$request->level;

        // dd($users);
        $users->save();
        return redirect('dashboard/user/')->with('success','User added');
    }
    public function edit($id)
    {
        $data = User::find($id);
        return view('admin.user.edit',['row' => $data]);
    }
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->level =$request->level;

        $user->save();
        return redirect('dashboard/user/')->with('success','User updated !');
    }
    public function delete($id)
    {
    	User::destroy($id);
        return redirect('dashboard/user/')->with('success','User deleted');
    }
}
