<?php

namespace App\Http\Controllers\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Video;
use App\Category;
use Illuminate\Support\Facades\DB;

class VideoController extends Controller
{
    public function home(){
    	$videos = Video::orderBy('id','desc')->paginate(5);
        $select_category = Category::all();    
    	return view('admin.video.home', compact('videos','select_category'));

    }

      public function create(Request $request)
    {
        $this->validate($request,[
            'name'           =>'required|string|max:50',
            'category_id'    =>'required|integer',
            'link'          =>'required|string',
        ]);

        $video = new Video;

        $video->name = $request->name;
        $video->category_id = $request->category_id;
        $video->link = $request->link;
        
        // dd($video);
        $video->save();
        return redirect('dashboard/video/')->with('success','video had to input');
    }

    public function edit($id)
    {
        $data = Video::find($id);
        $select_category = Category::all();
        // dd($data);
 
        // return view('admin.product.edit',(['row'=>$data]));
        return view('admin.video.edit',['row' => $data],compact('select_category'));
    }

    public function update(Request $request,$id)
    {
       $this->validate($request,[
            'name'          =>'required|string|max:50',
            'category_id'   =>'required|integer',
            'link'         =>'required|string',
        ]);

        $video = Video::find($id);
       
        $video->name = $request->name;
        $video->category_id = $request->category_id;
        $video->link = $request->link;

        $video->save();
        return redirect('dashboard/video/')->with('success','Data has been Success updated!');
        
    }


    public function delete($id)
    {
        Video::destroy($id);
        return redirect('dashboard/video/')->with('success','Video deleted');
    }
}
