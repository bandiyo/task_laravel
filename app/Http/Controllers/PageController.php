<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function contact(){
    	return view('halaman.contact');
    }

    public function howtoorder(){
    	return view('halaman.howtoorder');
    }

    public function informasi(){
    	return view('halaman.informasi');
    }
}
