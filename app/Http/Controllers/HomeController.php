<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Rent_video;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    // public function index()
    // {
    //     $products = Product::orderBy('id','desc')->paginate(8);
    //     // dd($product);

    //     return view('home.home',compact('products'));
    // }

    public function index()
    {
        $videos = Rent_video::find(1);
        // dd($videos);
        return view('home.home');
    }
}
