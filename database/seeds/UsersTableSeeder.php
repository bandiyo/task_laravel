<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	 DB::table('users')->insert([
	            'name' => 'admin',
                'email' => 'admin@gmail.com ',
	            'password' => bcrypt('admin123'),
	            'level'    => 'admin'     
	        ]);
    	 DB::table('users')->insert([
	            'name' => 'member',
                'email' => 'member@gmail.com ',
	            'password' => bcrypt('member123'), 
	            'level'    => 'member'            
	        ]);
    }
}
