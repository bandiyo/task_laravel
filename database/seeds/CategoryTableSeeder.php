<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
           
                DB::table('categories')->insert([
                            'name'=>'drama',
                            'sort'=>1          
                ]);
                
                DB::table('categories')->insert([
                            'name'=>'action',
                            'sort'=>2          
                ]);

                DB::table('categories')->insert([
                            'name'=>'comedy',
                            'sort'=>3          
                ]);

                 DB::table('categories')->insert([
                            'name'=>'music',
                            'sort'=>4          
                ]);

    }
}
