<?php

use Illuminate\Database\Seeder;

class VideoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('videos')->insert([
	            'name' => 'Goblin',
                'category_id' => '1',
	            'link' => 'https://www.youtube.com/watch?v=wpTVGgf6V1w',   
	        ]);
    }
}
